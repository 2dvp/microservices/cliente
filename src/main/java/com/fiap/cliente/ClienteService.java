package com.fiap.cliente;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {
    @Autowired
    ClienteRepository clienteRepository;

    public List<Cliente> getAllViews() {
        List<Cliente> clientes = new ArrayList<Cliente>();
        clienteRepository.findAll().forEach(cliente -> clientes.add(cliente));
        return clientes;
    }

    public Cliente getViewById(int id) {
        return clienteRepository.findById(id).get();
    }

    public void saveOrUpdate(Cliente cliente) {
        clienteRepository.save(cliente);
    }

    public void delete(int id) {
        clienteRepository.deleteById(id);
    }
}