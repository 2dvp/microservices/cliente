package com.fiap.cliente;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CLIENTE")
public class Cliente {
    @Id
    @GeneratedValue
    public int id;
    public String user_name;
    public String user_addrr; 
    public Integer getId() {
       return id;
   }  
}