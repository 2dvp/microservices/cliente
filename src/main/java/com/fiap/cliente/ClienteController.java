package com.fiap.cliente;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClienteController {
    @Autowired
    ClienteService clienteService;

    @GetMapping("/cliente")
    private List<Cliente> getAllViews() {
        return clienteService.getAllViews();
    }

    @GetMapping("/cliente/{id}")
    private Cliente getView(@PathVariable("id") int id) {
        return clienteService.getViewById(id);
    }

    @DeleteMapping("/cliente/{id}")
    private void deleteView(@PathVariable("id") int id) {
      clienteService.delete(id);
    }

    @PostMapping("/cliente")
    private int saveView(@RequestBody Cliente cliente) {
    clienteService.saveOrUpdate(cliente);
        return cliente.getId();
    }
}